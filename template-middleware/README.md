# Template for Lambda Projects

This project is intended to be used as skeleton when starts to develop lambdas on typescript and express


## Features

* Support for Typescript
* Express Framework
* Serverless Framework
* Test locally


## Local development

To run this project locally just run `sls offline` or `sls offline start`

