import { StatusRepository } from "../../repository/status.repository";
import { BaseBo } from "./abstractBase.bo";

export class StatusBo extends BaseBo<StatusRepository> {

    constructor() {
        super()
    }

}